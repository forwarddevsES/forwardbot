module.exports = {
    name: 'reglasdis',
    description: 'Reglas discord',
    cooldown: 5,
    execute(message) {
      message.channel.send({embed: {
          color: 3447003,
          author: {
            name: "Reglas del Discord",
            icon_url: "http://www.fondos12.com/data/media/2/big/patron-cuadrados-azules-23935-2560x1600__wallpaper_480x300.jpg",
          },
          fields: [{
            name: "Los administradores no están obligados a explicar la causa de tu baneo. Si has sido expulsado, ha sido por tu culpa. No se puede apelar un baneo de Discord; crear temas en el foro sobre esto solo aumentará el castigo.",
            value: "1.- Sé respetuoso con los demás usuarios de la comunidad.\n2.- No SPAM, mensajes repetidos, stream, links, canales de discord...\n3.- No MAYÚSCULAS, no es necesario ni bonito. \n4.- No flood (mensajes repetidos o simultáneos sin sentido alguno)\n5.- No contenido NSFW (Contenido sexual o de otra índole). \n6.- Respeto al STAFF, ellos están para ayudarte."
          }],
          footer: {
            icon_url: "https://i.imgur.com/TsO3pUa.pngW",
            text: "© SCUMRP.es 2018"
          }
        }
      });
    },
};

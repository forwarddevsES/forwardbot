module.exports = {
    name: 'infoservidor',
    description: 'Información del servidor actual.',
  	aliases: ['infoserver', 'infos'],
    guildOnly: true,
    cooldown: 2,
    execute(message, args) {
      message.channel.send({embed: {
        color: 3447003,
        description: `**Información del servidor**\n\nNombre del servidor: ${message.guild.name}\nUsuarios totales: ${message.guild.memberCount}\nCreado el: ${message.guild.createdAt}\nRegión del servidor: ${message.guild.region}`}});
     },
};

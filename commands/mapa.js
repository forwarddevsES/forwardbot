module.exports = {
    name: 'mapa',
    description: 'Información sobre soporte de ForwardBOT',
    cooldown: 5,
    execute(message) {
      message.channel.send({embed: {
          color: 3447003,
          author: {
            name: "Mapa del servidor",
            icon_url: "http://www.fondos12.com/data/media/2/big/patron-cuadrados-azules-23935-2560x1600__wallpaper_480x300.jpg",
          },
          title: "Haz click aquí para ver el mapa del servidor.",
          url: "http://scumrp.es",
          footer: {
            icon_url: "https://i.imgur.com/TsO3pUa.pngW",
            text: "© SCUMRP.es 2018"
          }
        }
      });
    },
};

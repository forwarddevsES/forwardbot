module.exports = {
    name: 'soporte',
    description: 'Información sobre soporte de ForwardBOT',
    cooldown: 5,
    execute(message) {
        message.channel.send('¿Necesitas soporte sobre el BOT? Puedes pedir soporte en: https://discord.gg/9FEZbFK (Canal #forwardbot)');
    },
};

module.exports = {
    name: 'play',
    description: 'SISTEMA EN PROCESO – NO DISPONIBLE',
    cooldown: 5,
    execute(message) {
      if (message.channel.type !== 'text') return;

      const { voiceChannel } = message.member;

      if (!voiceChannel) {
          return message.reply('únete primero a un canal de voz');
      }

      voiceChannel.join().then(connection => {
          const stream = ytdl('https://www.youtube.com/watch?v=ar9qA3WJInk', { filter: 'audioonly' });
          const dispatcher = connection.playStream(stream);

          dispatcher.on('end', () => voiceChannel.leave());
      });    },
};

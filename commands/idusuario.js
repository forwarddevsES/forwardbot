module.exports = {
	name: 'idusuario',
	description: 'Obtén la ID de un usuario.',
	aliases: ['idu', 'userid'],
	cooldown: 2,
	execute(message) {
    const taggedUser = message.mentions.users.first();
    if (!message.mentions.users.size) {
    return message.reply('necesitas mencionar a un usuario para poder ejecutar este comando.');}
    message.channel.send({embed: {
      color: 3447003,
      description: `Discord ID de **${taggedUser.username}**\n\n ${taggedUser.id}`}});
	},
};

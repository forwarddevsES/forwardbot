module.exports = {
	name: 'borrarm',
	cooldown: 1,
	guildOnly: true,
	execute(message, args) {
		const amount = parseInt(args[0]) + 1;

		if (isNaN(amount)) {
			return message.reply('no es un número válido.');
		}
		else if (amount <= 1 || amount > 100) {
			return message.reply('tienes que poner un número entre el 1 y el 99.');
		}

		message.channel.bulkDelete(amount, true).catch(err => {
			console.error(err);
			message.channel.send('ha habido un error intentando eliminar mensajes en este canal.');
		});
	},
};

module.exports = {
	name: 'avatar',
	description: 'Obtén tu avatar o el de algún usuario.',
	aliases: ['fotoperfil', 'fotop'],
  usage: '[nombre de usuario] o solo el comando',
  cooldown: 2,
	execute(message) {
		if (!message.mentions.users.size) {
			return message.channel.send(`Tu avatar: ${message.author.displayAvatarURL}`);
		}

		const avatarList = message.mentions.users.map(user => {
			return `Avatar de ${user.username} ${user.displayAvatarURL}`;
		});

		message.channel.send(avatarList);
	},
};

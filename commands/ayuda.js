const { prefix } = require('../config.json');

module.exports = {
	name: 'ayuda',
	description: 'Lista de todos los comandos o ayuda sobre un comando específico.',
	aliases: ['help', 'cmds'],
	usage: '[nombre del comando]',
	cooldown: 3,
	execute(message, args) {
		const data = [];
		const { commands } = message.client;

		if (!args.length) {
			data.push('Lista de todos mis comandos disponibles:');
			data.push(commands.map(command => command.name).join(', '));
			data.push(`\n¡Puedes utilizar \`${prefix}ayuda [nombre del comando]\` para obtener información sobre un comando específico!`);

			return message.author.send(data, { split: true })
				.then(() => {
					if (message.channel.type === 'dm') return;
					message.reply('te he enviado una lista con todos mis comandos.');
				})
				.catch(error => {
					console.error(`No se ha podido enviar un mensaje a ${message.author.tag}.\n`, error);
					message.reply('parece que no puedo enviarte mensajes privados.');
				});
		}

		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

		if (!command) {
			return message.reply('no es un comando válido.');
		}

		data.push(`**Nombre:** ${command.name}`);

		if (command.aliases) data.push(`**Alias:** ${command.aliases.join(', ')}`);
		if (command.description) data.push(`**Descripción:** ${command.description}`);
		if (command.usage) data.push(`**Uso:** ${prefix}${command.name} ${command.usage}`);

		data.push(`**Tiempo de espera:** ${command.cooldown || 3} segundo(s)`);

		message.channel.send(data, { split: true });
	},
};

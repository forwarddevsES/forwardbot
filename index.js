const fs = require('fs');
const Discord = require('discord.js');
const ytdl = require('ytdl-core');
const { prefix, token, version } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

client.once('ready', () => {
    console.log(`ForwardBOT (v.${version} Alpha) iniciado correctamente.`);
    client.user.setActivity('SCUMRP.es', { type: 'WATCHING' })
});

client.on('message', message => { // Command Handler
  if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();

	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	if (!command) return;

	if (command.guildOnly && message.channel.type !== 'text') {
		return message.reply('No puedo ejecutar este comando en chats privados.');
	}

	if (command.args && !args.length) {
		let reply = `No has indicado ningún argumento, ${message.author}!`;

		if (command.usage) {
			reply += `\nLa forma de usar este comando es: \`${prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send(reply);
	}

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;

	if (timestamps.has(message.author.id)) {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`Por favor, espera ${timeLeft.toFixed(1)} segundos antes de usar \`${command.name}\`.`);
		}
	}

	timestamps.set(message.author.id, now);
	setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

	try {
		command.execute(message, args);
	}
	catch (error) {
		console.error(error);
		message.reply('ha habido un error al ejecutar este comando.');
	}

});

client.on('guildMemberAdd', member => { // Mensaje bienvenida
    let channel = member.guild.channels.find('name', 'bienvenida');
    let memberavatar = member.user.avatarURL
        if (!channel) return;
        let embed = new Discord.RichEmbed()
        .setColor('RANDOM')
        .setThumbnail(memberavatar)
        .addField(':raising_hand: | ¡Bienvenido!', `Bienvenido al discord oficial de SCUMRP.es, ${member}`)
        .addField(':bangbang: | Atención', "Recuerda leer la normativa del servidor en nuestra web (**www.scumrp.es**)")
        .addField(':family_mwgb: | Eres el miembro número', `${member.guild.memberCount}`)
        .setFooter(`SCUMRP.es, el mejor servidor roleplay hispano de SCUM.`)
        .setTimestamp()

        channel.sendEmbed(embed);
});

client.login(token);
